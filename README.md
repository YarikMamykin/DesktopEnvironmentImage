# DesktopEnvironmentImage

## Description
This repository contains docker image spec for [DesktopEnvironment](https://codeberg.org/YarikMamykin/DesktopEnvironment)
Image, when built successfully, creates standalone container with custom `user` that contains all necessary packages for
desktop environment. More details on package list can be found [here](https://codeberg.org/YarikMamykin/DesktopEnvironment)

### Build arguments
- DISTRO = name of base distribution (e.g. archlinux, ubuntu)
- GROUP = desired user group id
- USER = desired user id
- ROOT_REPO = link to repo, that contains [DesktopEnvironment](https://codeberg.org/YarikMamykin/DesktopEnvironment) packages and binaries

### Build cmd
This image can be built within next command (e.g. for archlinux):
```
docker build \
			 --build-arg USER=1000
			 --build-arg GROUP=1000
			 --build-arg DISTRO=archlinux 
			 --build-arg ROOT_REPO="https://codeberg.org/YarikMamykin" 
			 -t archlinux-desktop .
```
