ARG DISTRO

FROM ${DISTRO}:latest

ARG DISTRO
ARG GROUP
ARG USER
ARG ROOT_REPO

COPY helpers/${DISTRO}/help.sh /

RUN chmod +x /help.sh && /help.sh

RUN groupadd -g ${GROUP} user && \
		useradd -u ${USER} -mg user -s /bin/bash user && \
		passwd -d root && \
		passwd -d user

RUN git clone ${ROOT_REPO}/DesktopEnvironment && \
    mv DesktopEnvironment/ /home/user/DesktopEnvironment

RUN chmod -R +x /home/user/DesktopEnvironment/ && \
		chown -R user:user /home/user/DesktopEnvironment/

ENV DISTRO=${DISTRO}
ENV ROOT_REPO=${ROOT_REPO}
ENV SCRIPTS_ROOT=${SCRIPTS_ROOT}

RUN cd /home/user/DesktopEnvironment/ && ./preinstall.sh && ./install.sh

RUN chown -R user:user /home/user

USER user
WORKDIR /home/user
